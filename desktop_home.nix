{ config, pkgs, ... }:

{
  imports = 
    [
    ./common.nix
    ./i3desktop.nix
    ];

  home.packages = [
    pkgs.easyeffects #audio compressor etc
    pkgs.foot

    # OBS fucks up camera, try these to fix:
    pkgs.v4l-utils
    pkgs.guvcview

    #stuff for sway
    pkgs.qt5.qtwayland
    pkgs.dmenu-wayland
    pkgs.wayland
    pkgs.imv
    pkgs.swaybg
    pkgs.wl-clipboard

    # Xorg stuff:
    pkgs.arandr
    pkgs.xorg.xrandr
    pkgs.dmenu
    pkgs.feh
    pkgs.i3
    pkgs.i3status
    pkgs.imv
    pkgs.xclip
    pkgs.xdotool
    pkgs.xorg.xmodmap
    pkgs.xterm
    pkgs.xorg.xwininfo
  ];

  #xsession.windowManager.i3 = {enable = true;};

  home.file.".xinitrc".text = ''
    exec i3
  '';
  

  wayland.windowManager.sway = {
    enable = false; 
    xwayland = true;
    wrapperFeatures.gtk = true;
    extraSessionCommands = ''
      export __GLX_VENDOR_LIBRARY_NAME=amd
      export SDL_VIDEODRIVER=wayland
      export QT_QPA_PLATFORM=wayland #needs qt5.qtwayland in systemPackages
      export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
      export WLR_NO_HARDWARE_CURSORS=1
    '';
    config = {
      bars = [];
      window.border = 4;
      floating.border = 4;
      floating.modifier = "Mod4";
      floating.titlebar = false;
      focus.followMouse = "yes";
      focus.forceWrapping = false;
      focus.mouseWarping = true;
      focus.newWindow = "smart";
      menu = "PATH=$PATH:${config.xdg.dataHome}/../bin:~/bin ${pkgs.dmenu-wayland}/bin/dmenu-wl_run";
      fonts = {
        names = [ "mononoki Nerd Font Mono" ];
        style = "Regular";
        size = 19.0;
      };
      keybindings = {
       "Mod4+Return" = "exec foot";
       "Mod4+x" = "exec qutebrowser";
       "Mod4+d" = "exec dmenu-wl_run -i";
       "Mod4+Shift+q" = "kill";
       "Mod4+h" = "focus left";
       "Mod4+j" = " focus down";
       "Mod4+k" = " focus up";
       "Mod4+l" = " focus right";
       "Mod4+Left" = " focus left";
       "Mod4+Down" = " focus down";
       "Mod4+Up" = " focus up";
       "Mod4+Right" = " focus right";
       "Mod4+Shift+h" = " move left";
       "Mod4+Shift+j" = " move down";
       "Mod4+Shift+k" = " move up";
       "Mod4+Shift+l" = " move right";
       "Mod4+Shift+Left" = " move left";
       "Mod4+Shift+Down" = " move down";
       "Mod4+Shift+Up" = " move up";
       "Mod4+Shift+Right" = " move right";
       "Mod4+Shift+Tab" = " move scratchpad"; 
       "Mod4+Tab" = " scratchpad show";
       "Mod4+c" = "split toggle";
       "Mod4+b" = "split h";
       "Mod4+v" = "split v";
       "Mod4+f" = "fullscreen toggle";
       "Mod4+w" = "layout tabbed";
       "Mod4+e" = "layout toggle split";
       "Mod4+space" = "floating toggle";
       "Mod4+Shift+space" = "focus mode_toggle";
       "Mod4+a" = "focus parent";
       "Mod4+s" = "focus child";
       "Mod4+Shift+r" = "exec swaymsg reload";
       "Mod4+Shift+e" = "exec swaymsg exit";
      };
      output = {
        DP-1 = { pos = "1920 0"; };
        DP-2 = { pos = "3840 0"; };
        DP-3 = { pos = "0 0"; };
      };
    };
    extraConfig = ''
bindsym Mod4+r mode "resize"
#bindsym Mod4+d = "exec --no-startup-id dmenu-wl_run -i";

input 1241:5890:USB_Keyboard xkb_layout se
input * xkb_numlock enable
output * bg ~/images/wallpapers/black_hole.jpg fit #000000

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym Mod4+1 workspace number $ws1
bindsym Mod4+2 workspace number $ws2
bindsym Mod4+3 workspace number $ws3
bindsym Mod4+4 workspace number $ws4
bindsym Mod4+5 workspace number $ws5
bindsym Mod4+6 workspace number $ws6
bindsym Mod4+7 workspace number $ws7
bindsym Mod4+8 workspace number $ws8
bindsym Mod4+9 workspace number $ws9
bindsym Mod4+0 workspace number $ws10

# move focused container to workspace
bindsym Mod4+Shift+1 move container to workspace number $ws1
bindsym Mod4+Shift+2 move container to workspace number $ws2
bindsym Mod4+Shift+3 move container to workspace number $ws3
bindsym Mod4+Shift+4 move container to workspace number $ws4
bindsym Mod4+Shift+5 move container to workspace number $ws5
bindsym Mod4+Shift+6 move container to workspace number $ws6
bindsym Mod4+Shift+7 move container to workspace number $ws7
bindsym Mod4+Shift+8 move container to workspace number $ws8
bindsym Mod4+Shift+9 move container to workspace number $ws9
bindsym Mod4+Shift+0 move container to workspace number $ws10

# color theming
	# class                 border  backgr. text    indicator child_border
	client.focused          #cc0000 #008833 #ffffff #cc00cc   #009933 
	client.focused_inactive #333333 #5f676a #ffffff #999999   #5f676a
	client.unfocused        #333333 #222222 #888888 #999999   #222222
	client.urgent           #2f343a #900000 #ffffff #900000   #900000
	client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c
	
client.background       #ffffff

bar {
       	mode hide
         status_command while date +'%Y-%m-%d %H:%M:%S '; do sleep 1; done

			position top
			separator_symbol ":|:"		

		colors {
        #colorclass		
			background	#000000
        statusline 	#ffffff
        separator 	#666666

				#colorclass					border	backg		text	
        focused_workspace  #009933 #336600 #ffffff
        active_workspace   #333333 #5f676a #ffffff
        inactive_workspace #333333 #222222 #888888
        urgent_workspace   #2f343a #900000 #ffffff
        binding_mode       #2f343a #900000 #ffffff
    	}
}

        '';
  };
  programs.obs-studio.plugins = [ pkgs.obs-studio-plugins.wlrobs ];
}

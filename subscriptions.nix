
{ config, pkgs, ... }:

# Pyradio, youtube twitch and other subscriptions.

{
  home.file.".config/ytcc/subscriptions.csv".text = ''
 name                        │ url                                                                             │ tags │ reverse 
─────────────────────────────┼─────────────────────────────────────────────────────────────────────────────────┼──────┼─────────
 CriticalRole                │ https://youtube.com/c/criticalrole/videos                                       │      │ false   
 AdamMillard                 │ https://www.youtube.com/c/thefearalcarrot/videos                                │      │ false   
 UpandAtom                   │ https://www.youtube.com/c/UpandAtom/videos                                      │      │ false   
 Veratisium                  │ https://www.youtube.com/c/Veritasium/videos                                     │      │ false   
 RationalityRules            │ http://www.youtube.com/channel/UCqZMgLgGlYAWvSU8lZ9xiVg/videos                  │      │ false   
 ThaBeast                    │ https://www.youtube.com/c/Thabeast721/videos                                    │      │ false   
 SkillUp                     │ https://www.youtube.com/c/SkillUp/videos                                        │      │ false   
 PhilosophyTube              │ https://www.youtube.com/c/thephilosophytube/videos                              │      │ false   
 MentalOutlaw                │ https://www.youtube.com/c/mentaloutlaw/videos                                   │      │ false   
 LindsayEllis                │ https://www.youtube.com/c/lindsayellis/videos                                   │      │ false   
 Kurzgesagt                  │ https://www.youtube.com/c/inanutshell/videos                                    │      │ false   
 Noclip                      │ https://www.youtube.com/c/noclip/videos                                         │      │ false   
 NileRed                     │ https://www.youtube.com/c/nilered/videos                                        │      │ false   
 NileBlue                    │ https://www.youtube.com/c/nilered2/videos                                       │      │ false   
 MarishaRayGun               │ https://www.youtube.com/c/marisharaygun/videos                                  │      │ false   
 GirlfriendReviews           │ https://www.youtube.com/c/girlfriendreviews/videos                              │      │ false   
 DarylTalksGames             │ https://www.youtube.com/c/daryltalksgames/videos                                │      │ false   
 DailyDoseofInternet         │ https://www.youtube.com/c/DailyDoseofinternet/videos                            │      │ false   
 Contrapoints                │ https://www.youtube.com/c/Contrapoints/videos                                   │      │ false   
 CeaveGaming                 │ https://www.youtube.com/c/Ceavegaming/videos                                    │      │ false   
 JoeScott                    │ http://www.youtube.com/c/joescott/videos                                        │      │ false   
 GameChopsMusic              │ https://www.youtube.com/c/gamechops/videos                                      │      │ false   
 MentalOutlawGentoo          │ https://www.youtube.com/playlist?list=PL3cu45aM3C2CADmCYeVhS4KTVut9MoMc9        │      │ false   
 MetalBladeRacordsMusic      │ https://www.youtube.com/c/metalbladerecords/videos                              │      │ false   
 CenturyMediaRecordsMusic    │ https://www.youtube.com/c/centurymedia/videos                                   │      │ false   
 NuclearBlastRecordsMusic    │ https://www.youtube.com/nuclearblastrecords/videos                              │      │ false   
 SarahZ                      │ https://www.youtube.com/c/SarahZ/videos                                         │      │ false   
 SailingSVDelos              │ https://www.youtube.com/c/SVDelos/videos                                        │      │ false   
 We'reInHell                 │ https://www.youtube.com/c/wereinhell/videos                                     │      │ false   
 AntiCitizenX                │ https://www.youtube.com/c/AntiCitizenX/videos                                   │      │ false   
 CandyRatMusic               │ https://www.youtube.com/candyrat/videos                                         │      │ false   
 AdventuresOfAnOldSeadog     │ https://www.youtube.com/c/AdventuresofanoldSeadog/videos                        │      │ false   
 GeneticallyModifiedSkeptic  │ https://www.youtube.com/c/GeneticallyModifiedSkeptic/videos                     │      │ false   
 GinnyDi                     │ https://www.youtube.com/c/Ginnydi/videos                                        │      │ false   
 InternetCommentEtiquette    │ https://www.youtube.com/user/Commentiquette/videos                              │      │ false   
 LockpickingLawyer           │ https://www.youtube.com/c/lockpickinglawyer/videos                              │      │ false   
 AcademyofIdeas              │ https://www.youtube.com/c/AcademyofIdeas/videos                                 │      │ false   
 JulieNolke                  │ https://www.youtube.com/JulieNolke/videos                                       │      │ false   
 CassEris                    │ https://www.youtube.com/c/casseris/videos                                       │      │ false   
 JillBearup                  │ https://www.youtube.com/c/JillBearup/videos                                     │      │ false   
 JimBrowning                 │ https://www.youtube.com/c/JimBrowning/videos                                    │      │ false   
 MarkRober                   │ https://www.youtube.com/c/markrober/videos                                      │      │ false   
 Exurb1a                     │ https://www.youtube.com/c/Exurb1a/videos                                        │      │ false   
 HideousDivinityLV426Music   │ https://www.youtube.com/playlist?list=PLVWT5waYZYnla1LQ95Dix_RzL6ldeRGB6        │      │ false   
 DarkTranquillityMomentMusic │ https://www.youtube.com/playlist?list=PLVWT5waYZYnm2vMp0Gfled3saGURPjCEA        │      │ false   
 DarkFuneralMusic            │ https://www.youtube.com/playlist?list=PLVWT5waYZYnlsKDVRtVyOsSAsPvInRUtD        │      │ false   
 DarkTranquilityMusic        │ https://www.youtube.com/playlist?list=PLVWT5waYZYnk-WTLDdANQ_Zl2n5v66L-8        │      │ false   
 AtTheGatesMusic             │ https://www.youtube.com/playlist?list=PLVWT5waYZYnltcAIyw2y4bkHPqM7MYts8        │      │ false   
 Razbuten                    │ https://www.youtube.com/c/Razbuten/videos                                       │      │ false   
 Computerphile               │ https://youtube.com/channel/UC9-y-6csu5WGm29I7JiwpnA/videos                     │      │ false   
 TheHatedOne                 │ https://youtube.com/channel/UCjr2bPAyPV7t35MvcgT3W8Q/videos                     │      │ false   
 PhysicsGirl                 │ https://youtube.com/channel/UC7DdEm33SyaTDtWYGO2CwdA/videos                     │      │ false   
 MightyVibesMusic            │ https://www.youtube.com/playlist?list=PL1tiwbzkOjQxZ08mDmvgp3aZEeOI51PA7        │      │ false   
 CodysLab                    │ https://www.youtube.com/channel/UCu6mSoMNzHQiBIOCkHUa2Aw/videos                 │      │ false   
 LinuxExperiment             │ https://youtube.com/channel/UC5UAwBUum7CPN5buc-_N1Fw/videos                     │      │ false   
 DistroTube                  │ https://youtube.com/channel/UCVls1GmFKf6WlTraIb_IaJg/videos                     │      │ false   
 ThatChapter                 │ https://www.youtube.com/channel/UCL44k-cLrlsdr7PYuMU4yIw/videos                 │      │ false   
 JCSCriminalPsychology       │ https://www.youtube.com/channel/UCYwVxWpjeKFWwu8TML-Te9A/videos                 │      │ false   
 HealthyGamerGG              │ https://youtube.com/channel/UClHVl2N3jPEbkNJVx-ItQIQ/videos                     │      │ false   
 LEMMiNO                     │ https://youtube.com/channel/UCRcgy6GzDeccI7dkbbBna3Q/videos                     │      │ false   
 BeauMiles                   │ https://youtube.com/channel/UCm325cMiw9B15xl22_gr6Dw/videos                     │      │ false   
 TomScott                    │ https://youtube.com/channel/UCBa659QWEk1AI4Tg--mrJ2A/videos                     │      │ false   
 GamesDoneQuick              │ https://youtube.com/channel/UCI3DTtB-a3fJPjKtQ5kYHfA/videos                     │      │ false   
 SsethTzeentach              │ https://youtube.com/channel/UCD6VugMZKRhSyzWEWA9W2fg/videos                     │      │ false   
 exurb2a                     │ https://youtube.com/channel/UCcoO-8J0EYQHGPFQqwmAzVQ/videos                     │      │ false   
 LinusTechTips               │ https://youtube.com/channel/UCXuqSBlHAE6Xw-yeJA0Tunw/videos                     │      │ false   
 EngineerMan                 │ https://youtube.com/channel/UCrUL8K81R4VBzm-KOYwrcxQ/videos                     │      │ false   
 BrianLunduke                │ https://youtube.com/channel/UCkK9UDm_ZNrq_rIXCz3xCGA/videos                     │      │ false   
 MCPappan                    │ https://youtube.com/channel/UCGR_5aLqOg_DbZgf7F1VnrA                            │      │ false   
 DanielForsen                │ https://youtube.com/channel/UCIEq1xfC2YHNjhe6Ib5cdDw                            │      │ false   
 JacobGeller                 │ https://youtube.com/channel/UCeTfBygNb1TahcNpZyELO8g/                           │      │ false   
 NeverKnowsBest              │ https://youtube.com/channel/UC1fKT0wuhchtclPqpdWEnHw/videos                     │      │ false   
 PBSIdea                     │ https://youtube.com/channel/UC3LqW4ijMoENQ2Wv17ZrFJA/videos                     │      │ false   
 Noodle                      │ https://youtube.com/channel/UCj74rJ9Lgl3WTngq675wxKg/videos                     │      │ false   
 GoodTimesBadTimes           │ https://youtube.com/channel/UCXW9oUSOwt7mcTT5d_5hQcA/videos                     │      │ false   
 AdamRagusea                 │ https://www.youtube.com/user/aragusea/videos                                    │      │ false   
 SpeakTheTruth               │ https://www.youtube.com/channel/UCvQ9wJKXJwcw_4tNaHHbWdA/videos                 │      │ false   
 JupiterBroadCasting         │ https://www.youtube.com/c/JupiterBroadcasting/videos                            │      │ false   
 KateBushMusic               │ https://www.youtube.com/channel/UC31hY7kg7CUOBpdyn9d_gYA/videos                 │      │ false   
 UberDanger                  │ https://www.youtube.com/user/UberDanger/videos                                  │      │ false   
 TheEscapistMagazine         │ https://www.youtube.com/user/TheEscapistMagazine/videos                         │      │ false   
 SphereHunter                │ https://www.youtube.com/channel/UCJhsl2VeLfSXzkutwlICyZw/videos                 │      │ false   
 HBomberGuy                  │ https://www.youtube.com/c/hbomberguy/videos                                     │      │ false   
 GamingOnLinux               │ https://www.youtube.com/channel/UCm9lqWLMec4klbLEtUxKSbg/videos                 │      │ false   
 WolfgangsChannel            │ https://www.youtube.com/channel/UCsnGwSIHyoYN0kiINAGUKxg/videos                 │      │ false   
 TomMcReaMusic               │ https://www.youtube.com/playlist?list=OLAK5uy_nIkg5MBXjCYuh3S9Vn40GYq7LIeqNhTyk │      │ false   
 TomMcReaJustLikeBloodMusic  │ https://www.youtube.com/playlist?list=OLAK5uy_kKlilTYYHeVA1xKkVJi5MaP-8PBA7ikwY │      │ false   
    '';


  home.file.".config/pyradio/mystations.csv".text = ''
# Find lots more stations at http://www.iheart.com,
Alternative (BAGeL Radio),https://ais-sa3.cdnstream1.com/2606_128.aac
Alternative (The Alternative Project),http://c9.prod.playlists.ihrhls.com/4447/playlist.m3u8
Beyond Metal (Progressive - Symphonic),http://streamingV2.shoutcast.com/BeyondMetal
DanceUK,https://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://uk2.internet-radio.com:8024/listen.pls&t=.pls
Echoes of Bluemars,http://streams.echoesofbluemars.org:8000/bluemars.m3u
Echoes of Bluemars - Cryosleep,http://streams.echoesofbluemars.org:8000/cryosleep.m3u
Echoes of Bluemars - Voices from Within,http://streams.echoesofbluemars.org:8000/voicesfromwithin.m3u
Electronic/Dance (Electronic Culture),http://www.shouted.fm/tunein/electro-dsl.m3u
Hip Hop (Hot 97 NYC),http://playerservices.streamtheworld.com/pls/WQHTAAC.pls
Hip Hop (Power 1051 NYC),http://c11.prod.playlists.ihrhls.com/1481/playlist.m3u8
JazzGroove,https://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://199.180.72.2:8015/listen.pls?sid=1&t=.pls
Pop/Rock/Urban  (Frequence 3 - Paris) ,http://streams.frequence3.net/hd-mp3.m3u
Public Radio (WNYC - Public Radio from New York to the World),http://wnyc-iheart.streamguys.com/wnycfm-iheart.aac
Radio Paradise - Main Mix,http://stream.radioparadise.com/aac-128
Radio Paradise - Mellow Mix,http://stream.radioparadise.com/mellow-128
Radio Paradise - Rock Mix,http://stream.radioparadise.com/rock-128
Radio Paradise - Eclectic Mix,http://stream.radioparadise.com/eclectic-128
Reggae Dancehall (Ragga Kings),http://www.raggakings.net/listen.m3u
Slay Radio (Commodore  64 remix),http://www.slayradio.org/tune_in.php/128kbps/listen.m3u
Vox Noctem: Rock-Goth, http://r2d2.voxnoctem.de:8000/voxnoctem.mp3
Beat Blender SomaFM,https://somafm.com/beatblender.pls
Boot Liqour SomaFM,https://somafm.com/bootliquor.pls
BRFM SomaFM,https://somafm.com/brfm.pls
Cliq Hop SomaFM,https://somafm.com/cliqhop.pls
Covers SomaFM,https://somafm.com/covers.pls
Defcon SomaFM,https://somafm.com/defcon.pls
Digitalis SomaFM,https://somafm.com/digitalis.pls
Drone Zone SomaFM,https://somafm.com/dronezone.pls
Deep Space One SomaFM,https://somafm.com/deepspaceone.pls
Dubstep SomaFM,https://somafm.com/dubstep.pls
Fluid SomaFM,https://somafm.com/fluid.pls
FolkFWD SomaFM,https://somafm.com/folkfwd.pls
Ill Street SomaFM,https://somafm.com/illstreet.pls
Indie Pop SomaFM,https://somafm.com/indiepop.pls
GSR classic SomaFM,https://somafm.com/gsclassic.pls
Groove Salad SomaFM,https://somafm.com/groovesalad.pls
Live SomaFM,https://somafm.com/live.pls
Lush SomaFM,https://somafm.com/lush.pls
Metal SomaFM,https://somafm.com/metal.pls
Mission Control SomaFM,https://somafm.com/missioncontrol.pls
Pop Tron SomaFM,https://somafm.com/poptron.pls
Reggae SomaFM,https://somafm.com/reggae.pls
Scanner SomaFM,https://somafm.com/scanner.pls
Secret Agent SomaFM,https://somafm.com/secretagent.pls
Seventies SomaFM,https://somafm.com/seventies.pls
Sonic Universe SomaFM,https://somafm.com/sonicuniverse.pls
Space Station SomaFM,https://somafm.com/spacestation.pls
Specials SomaFM,https://somafm.com/specials.pls
Suburbs of Goa SomaFM,https://somafm.com/suburbsofgoa.pls
Synphaera SomaFM,https://somafm.com/synphaera.pls
Tags Trance SomaFM,http://somafm.com/tagstrance.pls
The Trip SomaFM,https://somafm.com/thetrip.pls
Thistle SomaFM,https://somafm.com/thistle.pls
Vapor Waves SomaFM,https://somafm.com/vaporwaves.pls
SR P1,https://live-cdn.sr.se/pool2/p1/p1.isml/p1-audio=192000.m3u8
SR P2,https://live-cdn.sr.sr/pool2/p2musik/p2musik.isml/p2musik-audio=192000.m3u8
SR P3,https://live-cdn.sr.se/pool1/p3/p3.isml/p3-audio=192000.m3u8
SR P4 Kronoberg,https://live-cdn.sr.se/pool3/p4kronoberg/p4kronoberg.isml/p4kronoberg-audio=192000.m3u8
SR P3 Din Gata,https://live-cdn.sr.se/pool3/dingata/dingata.isml/dingata-audio=192000.m3u8
SR P4 Plus,https://live-cdn.sr.se/pool2/srextra17/srextra17.isml/srextra17-audio=192000.m3u8
    '';



}

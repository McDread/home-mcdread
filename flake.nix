{
  description = "McDread's personal home-manager flake.";
  
  inputs = {
    nixos.url = "github:nixos/nixpkgs/nixos-unstable";
    home = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixos";
    };
  };

  outputs = inputs: {
      homeConfigurations."nixos-desktop" = inputs.home.lib.homeManagerConfiguration {
        pkgs = inputs.nixos.legacyPackages.x86_64-linux;
        modules = [
          ./desktop_home.nix
          {
            home = {
              username = "mcdread";
              homeDirectory = "/home/mcdread";
              stateVersion = "21.11"; #22.05
            };
          }
        ];
      };

      homeConfigurations."nixos-laptop" = inputs.home.lib.homeManagerConfiguration {
        pkgs = inputs.nixos.legacyPackages.x86_64-linux;
        modules = [
          ./laptop_home.nix
          {
            home = {
              username = "mcdread";
              homeDirectory = "/home/mcdread";
              stateVersion = "21.11";
            };
          }
        ];
      };
        #system = "x86_64-linux";
        #pkgs = inputs.nixos.legacyPackages.x86_64-linux;
        #homeDirectory = "/home/mcdread";
        #username = "mcdread";
        #stateVersion = "21.11";
        #configuration = { pkgs, ... }: {
        #  imports = [ ./laptop_home.nix ];
        #};
      #};

      homeConfigurations."nixos-desktop-old" = inputs.home.lib.homeManagerConfiguration {
        pkgs = inputs.nixos.legacyPackages.x86_64-linux;
        modules = [
          ./laptop_home.nix
          {
            home = {
              username = "mcdread";
              homeDirectory = "/home/mcdread";
              stateVersion = "21.11";
            };
          }
        ];
      };
    #defaultPackage."nixos-desktop" = self.home.homeConfigurations."nixos-desktop".activationPackage;
    #defaultPackage."nixos-laptop" = self.home.homeConfigurations."nixos-laptop".activationPackage;
    #nixos-desktop = self.homeConfigurations.nixos-desktop.activationPackage;
    #nixos-laptop = self.homeConfigurations.nixos-laptop.activationPackage;
  };
}

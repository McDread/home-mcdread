{ config, pkgs, ... }:

# man home-configuration.nix(5) to list all options
# man page will show e.g: Declared by: <home-manager/modules/programs/mpv.nix>
# https://github.com/nix-community/home-manager/blob/master/modules/programs/mpv.nix

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  imports = [ ./subscriptions.nix ];
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.sessionPath = [ "$HOME/bin" ];
  home.username = "mcdread";
  home.homeDirectory = "/home/mcdread";

  nixpkgs.config.allowUnfree = true;

  home.packages = with pkgs; [
    agrep
    amfora
    bombadillo
    bitwarden
    bitwarden-cli
    castget
    chatterino2
    duckling-proxy # Make http/s into gemini
    freetube
    glxinfo
    godot
    groff
    kitty
    legendary-gl
    nyxt
    obsidian
    pulsemixer
    pipe-viewer
    pscircle
    pyradio
    spotdl
    steamcmd
    steam-tui
    streamlink
    streamlink-twitch-gui-bin
    surfraw
    sqlitebrowser
    unzip
    vkBasalt
    wine-staging
    wine64
    winetricks
    xst
    youtube-dl
    ytcc
    yt-dlp
    ytfzf
    zathura #pdf viewer

    # Non free
    bottles
    itch
    lutris
    steam-run
  ];

  programs.go = {
    enable = true;
  };

  home.shellAliases = {
    ".." = "cd ..";
    dd = "dd bs=8192";
    groff = "groff -Kutf8 -ms -s";
    ls = "ls -lh --color=auto";
    lynx = "lynx https://lite.duckduckgo.com/lite/";
    links = "links https://lite.duckduckgo.com/lite/";
    rm = "rm -I";
    scim = "sc-im"; 
  };

  programs.zsh = {
    defaultKeymap = "viins";
    dotDir = ".config/zsh";
    enable = true;
    enableAutosuggestions = true;
    enableSyntaxHighlighting = true;
    history = {
      expireDuplicatesFirst = true;
      ignoreDups = true;
      ignorePatterns = [ "rm *" "mpv .*" ];
    };
    initExtra = ''
      autoload -Uz vcs_info
      precmd () { vcs_info }
      zstyle ':vcs_info:' formats '(%b)'
      #$vcs_info_msg_0_
      PS1='%F{blue}[%F{red}%T%F{blue}][%F{green}%1~%F{blue}]%f %F{yellow}%B$ '
    '';
  };

  programs.git = {
    enable = true;
    userName = "mcdread";
    userEmail = "mcdread@protonmail.com";
    lfs.enable = true;
    extraConfig = {
      safe.directory = "/home/mcdread/stuff/nixos-mcdread";
    };
  };

  programs.bash = {
    enable = false;
    bashrcExtra = ''
    export PS1="\[$(tput bold)\]\[\033[38;5;21m\][\[$(tput sgr0)\]\[\033[38;5;160m\]\A\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;2m\]\W\[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;21m\]]\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;3m\]\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"
      set -o vi

      '';
  };

  # Needed for surfraw finding config
  xdg.systemDirs.config = [ "~/.config" ];
  xdg.mimeApps = {
    enable = true;
    # For availible apps run: ls /run/current-system/sw/share/applications 
    defaultApplications = {
      "text/html" = [ "org.qutebrowser.qutebrowser.desktop" "lynx.desktop" ];
      "x-scheme-handler/http" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/https" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/about" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/unknown" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/pdf" = [ "org.pwmt.zathura.desktop" ];
      "image/png" = [ "imv.desktop" "feh.desktop" ];
      "image/jpeg" = [ "imv.desktop" "feh.desktop" ];
      "image/jwebp" = [ "imv.desktop" "feh.desktop" ];
      "image/gif" = [ "imv.desktop" "feh.desktop" ];
    };
  };

  programs.neovim = {
    enable = true;
    #coc.enable = true;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    plugins = with pkgs.vimPlugins; [ vim-lastplace vim-nix vim-hexokinase 
        nvim-fzf nvim-autopairs rainbow ];
    extraPackages = with pkgs; [ nodejs ];
    extraConfig = ''
      set nocompatible
      set backspace=indent,eol,start
      set hlsearch
      set list
      set number ruler
      set tabstop=3 shiftwidth=3 expandtab
      set clipboard=unnamedplus
      set termguicolors
      let g:Hexokinase_highlighters = [ 'backgroundfull' ]
    '';
  };

  programs.kakoune = {
    enable = true;
    config = {
      indentWidth = 2;
      numberLines.enable = true;
      numberLines.highlightCursor = true;
      ui.assistant = "cat";
      wrapLines.enable = true;
    };
    plugins = with pkgs.kakounePlugins; [
      auto-pairs-kak fzf-kak kak-lsp kakboard kakoune-extra-filetypes 
      kakoune-rainbow kakoune-state-save kakoune-vertical-selection
    ];
    extraConfig = ''
      eval %sh{kak-lsp --kakoune -s $kak_session}  # Not needed if you load it with plug.kak.
      lsp-enable
      hook global WinCreate .* %{ kakboard-enable }
      map global user v     ': vertical-selection-down<ret>'
      map global user <a-v> ': vertical-selection-up<ret>'
      map global user V     ': vertical-selection-up-and-down<ret>'
      map global user f ': fzf-mode<ret>'
      '';
  };

  programs.mpv.enable = true; # Enable the mpv media player
  programs.mpv.scripts = with pkgs.mpvScripts;
    [
      sponsorblock # Skip sponsored segments of YouTube videos
    ];
  programs.mpv.bindings = {
    WHEEL_UP = "add volume +5";
    WHEEL_DOWN = "add volume -5";
  };
  programs.mpv.profiles = {
    pyradio = {
      volume = "70";
    };
  };
  programs.mpv.config = {
    audio-display = "no"; # No album art when playing audio
    #cscale = "ewa_lanczossharp";
    #deinterlace = "yes";
    demuxer-max-bytes = "3GiB";
    demuxer-max-back-bytes = "4GiB";
    #dscale = "mitchell";
    fullscreen = "yes";
    #gpu-context = "wayland";
    hwdec = "auto-safe";
    hwdec-codecs = "all";
    #interpolation = "yes";
    keep-open = "always";
    #msg-level = "vo=fatal"; # Prevent harmless warnings/errors when using hardware decoding
    opengl-es = "yes";
    #profile = "gpu-hq";
    #scale = "ewa_lanczossharp"; # If your hardware can run it, this is probably what you should use by default
    #scaler-resizes-only = "yes"; # Disable the scaler if the video image is not resized
    #script-opts = "ytdl_hook-ytdl_path=yt-dlp"; # Use yt-dlp intstead of youtube-dl
    script-opts = ''ytdl_hook-ytdl_path=yt-dlp,sponsorblock-skip_categories="sponsor,intro,outro,interaction,selfpromo,preview",sponsorblock-local_database=no'';
    #tscale = "oversample"; # This filter is good at temporal interpolation
    #video-sync = "display-vdrop"; # Drop or repeat video frames to compensate desyncing video
    vo = "gpu"; # Enable hardware acceleration
    #ytdl-format = "bestvideo[height<=?720][fps<=?30][vcodec!=?vp9]+bestaudio/best";
  };
  

  programs.qutebrowser = {
    enable = true;
    searchEngines = {
      anydeal = "https://isthereanydeal.com/search/?q={}";
      arch = "https://wiki.archlinux.org/index.php?search={}";
      ddgB = "https://duckduckgo.com/?q={}";
      ddg = "https://lite.duckduckgo.com/lite/?q={}"; 
      fh = "https://flathub.org/apps/search/{}";
      gh = "https://github.com/search?q={}";
      hoogle = "https://hoogle.haskell.org/?hoogle={}";
      nix = "https://search.nixos.org/packages?channel=unstable&query={}";
      nixpkgs = "https://github.com/NixOS/nixpkgs/issues?q={}";
      map = "https://www.openstreetmap.org/search?query={}";
      odysee = "https://odysee.com/$/search?q={}";
      poe = "https://www.poewiki.net/w/index.php?search={}";
      proton = "https://www.protondb.com/search?q={}";
      sp = "https://www.startpage.com/sp/search?query={}";
      DEFAULT = "https://searx.be/search?q={}"; # Update from https://searx.space
      steam = "https://store.steampowered.com/search/?term={}";
      tyda = "https://tyda.se/search/{}";
      word = "https://www.wordnik.com/words/{}";
      wiki = "https://en.wikipedia.org/w/index.php?search={}";
      wine = "https://www.winehq.org/search?q={}";
      yt = "https://piped.silkky.cloud/results?search_query={}";
    };
    extraConfig = ''
      config.bind('<Ctrl-Shift-V>', 'spawn mpv {url}')
      config.bind('<Ctrl-v>', 'hint links spawn mpv {hint-url}')
      #config.set("colors.webpage.darkmode.enabled", True)
      config.set("colors.webpage.bg", "black")
      config.set("content.user_stylesheets", "~/.config/qutebrowser/css/black.css")
      config.bind('<Ctrl-d>', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/css/black.css ~/.config/qutebrowser/css/darculized-all-sites.css')
      config.bind('<Ctrl-Shift-D>', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/css/black.css ""')
      config.set("auto_save.session", True)
      config.set("content.notifications.enabled", False)
      config.set("content.blocking.method", "both")
      c.content.blocking.adblock.lists = ['https://easylist.to/easylist/easylist.txt', 'https://easylist.to/easylist/easyprivacy.txt', 'https://easylist-downloads.adblockplus.org/easylistdutch.txt', 'https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt', 'https://www.i-dont-care-about-cookies.eu/abp/', 'https://secure.fanboy.co.nz/fanboy-cookiemonster.txt']

      config.set("fonts.default_size", "10pt")
      config.set("fonts.web.size.default", 20)
    '';
  };

  home.file.".config/qutebrowser/greasemonkey/yt_add_skip.js".text = ''
// ==UserScript==
// @name         Auto Skip YouTube Ads 
// @version      1.0.1
// @description  Speed up and skip YouTube ads automatically 
// @author       jso8910
// @match        *://*.youtube.com/*
// @exclude      *://*.youtube.com/subscribe_embed?*
// ==/UserScript==
let main = new MutationObserver(() => {
    let ad = [...document.querySelectorAll('.ad-showing')][0];
    if (ad) {
        let btn = document.querySelector('.videoAdUiSkipButton,.ytp-ad-skip-button')
        if (btn) {
            btn.click()
        }
    }
})
main.observe(document.querySelector('.videoAdUiSkipButton,.ytp-ad-skip-button'), {attributes: true, characterData: true, childList: true})
  '';

  home.file.".config/qutebrowser/css/black.css".text = ''
*,html,div,p,td,tr,body,#header,html,:root,#main {
   background-color: black !important;
	background-image: none !important;
   color: #bbbbbb;
}

pre,code,input,select,button,.menu-item,.select-menu-item,.btn,textarea,.color-bg-default {
   background-color: #090909 !important;
	background-image: none !important;
   color: #00bb99;
}

h1,h2,h3,h4 {
	background-color: black !important;
	color: #e02020 !important;
}

a {
	color: #5050f0 !important;
	background-color: black !important;
}
  '';

  programs.obs-studio.enable = true;
  programs.obs-studio.plugins = [ pkgs.obs-studio-plugins.wlrobs ];

  home.file."bin/ytcc-oldest.sh" = {
    executable = true;
    text = ''
      #!/bin/sh
      if [ -z "$2" ] #add any argument to play marked
      then
        ID=$(ytcc -t no list -a publish_date,id,playlists,title | grep -i "$1" | sort | cut -d ' ' -f 6 | head -n 1)
      else
        ID=$(ytcc -t no list -w -a publish_date,id,playlists,title | grep -i "$1" | sort | cut -d ' ' -f 6 | head -n 1)
      fi

      if [ "$ID" = "" ]
      then
        echo "No id found, playing marked"
        ID=$(ytcc -t no list -w -a publish_date,id,playlists,title | grep -i "$1" | sort | cut -d ' ' -f 6 | head -n 1)
      fi

      if [ "$ID" = "" ]
      then
        echo "Sorry, still no id found"
      else
        ytcc play $ID
        echo "played ID:  $ID"
      fi
    '';
  };

  home.file.".config/zathura/zathurarc".text = ''
      set recolor true
      set adjust-open best-fit
      set selection-clipboard clipboard
    '';

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";
}

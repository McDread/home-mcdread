{ config, pkgs, ... }:

{
  imports = 
    [
    ./common.nix
    ./i3config.nix
    ];

  home.packages = [
    pkgs.dmenu
    pkgs.feh
    pkgs.i3
    pkgs.i3status
    pkgs.imv
    pkgs.libvdpau-va-gl
    pkgs.xclip
    pkgs.xdotool
    pkgs.xorg.xmodmap
    pkgs.xterm
  ];

  home.file.".xinitrc".text = ''
    exec i3
  '';

  home.file."bin/wallpaper_random.sh" = {
      executable = true;
      text = ''
        #!/bin/sh
        while true
        do
	       RND_IMG=`ls ~/images/wallpapers/ | shuf -n 1`
	       eval "feh --bg-max ~/images/wallpapers/$RND_IMG"
	       sleep 240
        done
      '';
  };

}

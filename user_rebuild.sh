#!/usr/bin/env sh
SCRIPT_PATH=$(dirname "$0")
warn=$(tput bold && tput setaf 0 && tput setab 7)
normal=$(tput sgr0)

echo "${warn}HOME REBUILD IMMINENT:${normal} Are you sure you want to?"
read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'

echo "rebuilding home environment..."

home-manager switch --flake "${SCRIPT_PATH}#$(hostname)" --impure
